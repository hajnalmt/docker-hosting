# Docker hosting

This project is the main repository for the Digitlean documentation.

We use Vuepress, to get started, please visit the [Vuepress Getting Started guide](https://vuepress.vuejs.org/guide/getting-started.html).

Or edit the markdown files under the docs folder.

### Node and NPM install

For the project you need Node and npm.

We use npm 6.14.13 and node v14.17.0.

To use them we recommend the usage of nvm (Node version manager.)

```
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | bash
```

#### Install nvm

To install nvm, enter this nice bash command.

```
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | bash
``` 

Check how the installation went:

```
nvm --version
> 0.38.0 
```

#### Install Node with nvm

To install node, enter this nice bash command.

```
nvm install v14.17.0
```

Then check for the versions.

```
node --version
> v14.17.0
npm --version
> 0.38.0
```

You are now ready to start! :rocket:

#### Run docs locally

First you need to install dependencies.

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:8080
$ npm run dev

# build for production
$ npm run build
```

### Docker install

The easiest and cleanest way to run the project through docker.
To install Docker Desktop visit, the [Docker Desktop overview page](https://docs.docker.com/desktop/)

If you have a local docker cli, and composer installed.

You can run a development server with the command:
```
docker-compose up
```

This will start the development server on port 8080

To build the application, a standard docker build is needed like:

```
docker build -t digitlean-docs:latest .
docker run -p 8080:80 digitlean-docs:latest
```

For the build the output nginx container uses port 80 as you can see.
