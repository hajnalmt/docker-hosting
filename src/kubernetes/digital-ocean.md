# Digital Ocean API usage

## Install Doctl

Download the archive from your browser or copy its URL and retrieve it to your home directory with wget.
```
cd ~
wget https://github.com/digitalocean/doctl/releases/download/v1.60.0/doctl-1.60.0-linux-amd64.tar.gz
```

Next, extract the binary. For example, to do so using tar, run:
```
tar xf ~/doctl-1.60.0-linux-amd64.tar.gz
```

Finally, move the doctl binary into your path by running:

```
sudo mv ~/doctl /usr/local/bin
```

## Initialize Doctl context

```
doctl auth init --context hajnalmt-do
```

Enter the api token provided and then check if the context has been created or not.

```
doctl auth list
```

You should see the hajnalmt-do context as an ouput. Let's select that context!

```
doctl auth switch --context hajnalmt-do
```

